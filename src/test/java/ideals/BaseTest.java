package ideals;

import ideals.core.ConciseAPI;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseTest extends ConciseAPI{

    public static WebDriver driver;
    public static WebDriverWait wait;

    @BeforeClass
    public static void createDriver() {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 6);
    }

    @AfterClass
    public static void quit() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }
}
