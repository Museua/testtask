package ideals;

import ideals.pages.GoogleMainPage;
import ideals.pages.GoogleSearchPage;
import ideals.pages.IdealsMainPage;
import org.junit.Test;

public class IdealsTest extends BaseTest {

    @Test
    public void basicIdealsTest(){
        open("http://www.google.com");
        googleMainPage.search("iDeals");
        googleSearchPage.followToLinkFromResults("https://www.idealsvdr.com/");
        idealsMainPagedealsMainPage.checkFontCollorButton("Log In", "rgba(112, 164, 41, 1)");
        idealsMainPagedealsMainPage.checkBackgroundCollorButton("Sign Up", "rgba(112, 164, 41, 1)");
        idealsMainPagedealsMainPage.checkFontCollorButton("Sign Up", "rgba(255, 255, 255, 1)");
        idealsMainPagedealsMainPage.checkTitle("iDeals™ Virtual Data Rooms | Secure Data Room Provider");
    }

    GoogleMainPage googleMainPage = new GoogleMainPage(driver);
    GoogleSearchPage googleSearchPage = new GoogleSearchPage(driver);
    IdealsMainPage idealsMainPagedealsMainPage = new IdealsMainPage(driver);

}
