package ideals.pages;

import ideals.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class IdealsMainPage extends BasePage{
    public IdealsMainPage(WebDriver driver){
        super(driver);
    }

    public void  checkFontCollorButton(String nameButton, String buttonFontColor){
        String actualButtonFontColor = new WebDriverWait(driver, 6).until(presenceOfElementLocated(By.xpath("//a[@title='"+nameButton+"']"))).getCssValue("color");
        if (!actualButtonFontColor.equals(buttonFontColor)){
            System.out.printf("The font color buttons on site not match to needed. Actual - " + actualButtonFontColor + ". Expected - " + buttonFontColor);
            return;
        }
    }

    public void  checkBackgroundCollorButton(String nameButton, String backgroundButtonColor){
        String actualBackgroundButtonColor = new WebDriverWait(driver, 6).until(presenceOfElementLocated(By.xpath("//a[@title='"+nameButton+"']"))).getCssValue("background-color");
        if (!actualBackgroundButtonColor.equals(backgroundButtonColor)){
            System.out.printf("The background color buttons on site not match to needed. Actual - " + actualBackgroundButtonColor + ". Expected - " + backgroundButtonColor);
            return;
        }
    }

    public void checkTitle(String title){
        String actualTitle = driver.getTitle();
        if (!actualTitle.equals(title)){
            System.out.printf("The title on site not match to needed. Actual - " + actualTitle + ". Expected - " + title);
            return;
        }
    }

}
