package ideals.pages;

import ideals.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;

public class GoogleMainPage  extends BasePage{

    public GoogleMainPage(WebDriver driver){
        super(driver);
    }

    public void  search(String searhText){
        new WebDriverWait(driver, 6).until(presenceOfElementLocated(By.name("q"))).sendKeys(searhText + Keys.ENTER);
    }
//    public void assertFindedResult(int  index, String mailHeaderText){
//        assertThat(nthElementHasText(mails, index, mailHeaderText));
//    }
}
