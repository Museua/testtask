package ideals.pages;

import ideals.core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class GoogleSearchPage extends BasePage {

    public GoogleSearchPage(WebDriver driver){
        super(driver);
    }

    public void  followToLinkFromResults(String searhLink){
        new WebDriverWait(driver, 6).until(presenceOfElementLocated(By.xpath("//h3/a[@href='" + searhLink + "']"))).click();
    }
}
