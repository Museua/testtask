package ideals.core;

import org.openqa.selenium.WebDriver;

public abstract class ConciseAPI {
    public abstract WebDriver getDriver();

    public void open(String url){
        getDriver().get(url);
    }
}
